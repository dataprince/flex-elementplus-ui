import request from '@/utils/request'

// 查询学生信息单表(mb)列表
export function listStudent(query) {
  return request({
    url: '/demo/student/list',
    method: 'get',
    params: query
  })
}

// 查询学生信息单表(mb)详细
export function getStudent(studentId) {
  return request({
    url: '/demo/student/' + studentId,
    method: 'get'
  })
}

// 新增学生信息单表(mb)
export function addStudent(data) {
  return request({
    url: '/demo/student',
    method: 'post',
    data: data
  })
}

// 修改学生信息单表(mb)
export function updateStudent(data) {
  return request({
    url: '/demo/student',
    method: 'put',
    data: data
  })
}

// 删除学生信息单表(mb)
export function delStudent(studentId) {
  return request({
    url: '/demo/student/' + studentId,
    method: 'delete'
  })
}
