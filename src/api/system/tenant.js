import request from '@/utils/request';
import {genKeyPair} from "@/api/genKeyPair";

const encryptHeader = import.meta.env.VITE_APP_ENCRYPT_HEADER;

// 查询租户列表
export function listTenant(query) {
  return request({
    url: '/system/tenant/list',
    method: 'get',
    params: query
  });
}

// 查询租户详细
export function getTenant(tenantId) {
  return request({
    url: '/system/tenant/' + tenantId,
    method: 'get'
  });
}

// 新增租户
export function addTenant(data) {
  return new Promise((resolve, reject) => {
    genKeyPair((uuid,public_key)=>{
      request({
        url: '/system/tenant',
        method: 'post',
        headers: {
          isEncrypt: true,
          [encryptHeader]: uuid,
          publicKey: public_key,
        },
        data: data
      }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  });
}

// 修改租户
export function updateTenant(data) {
  return request({
    url: '/system/tenant',
    method: 'put',
    data: data
  });
}

// 租户状态修改
export function changeTenantStatus(tenantId, version, status) {
  const data = {
    tenantId,
    version,
    status
  };
  return request({
    url: '/system/tenant/changeStatus',
    method: 'put',
    data: data
  });
}

// 删除租户
export function delTenant(tenantId) {
  return request({
    url: '/system/tenant/' + tenantId,
    method: 'delete'
  });
}

// 动态切换租户
export function dynamicTenant(tenantId) {
  return request({
    url: '/system/tenant/dynamic/' + tenantId,
    method: 'get'
  });
}

// 清除动态租户
export function dynamicClear() {
  return request({
    url: '/system/tenant/dynamic/clear',
    method: 'get'
  });
}

// 同步租户套餐
export function syncTenantPackage(tenantId, packageId) {
  const data = {
    tenantId,
    packageId
  };
  return request({
    url: '/system/tenant/syncTenantPackage',
    method: 'get',
    params: data
  });
}
