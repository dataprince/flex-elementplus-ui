import request from '@/utils/request'
import { parseStrEmpty } from "@/utils/ruoyi";
import {genKeyPair} from "@/api/genKeyPair";
const encryptHeader = import.meta.env.VITE_APP_ENCRYPT_HEADER;
// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/system/user/' + parseStrEmpty(userId),
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'delete'
  })
}

// 用户密码重置
export function resetUserPwd(userId, version, password) {
  const data = {
    userId,
    version,
    password
  }
  return new Promise((resolve, reject) => {
    genKeyPair((uuid,public_key)=>{
      request({
        url: '/system/user/resetPwd',
        method: 'put',
        headers: {
          isEncrypt: true,
          [encryptHeader]: uuid,
          publicKey: public_key,
        },
        data: data
      }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  })
}

// 用户状态修改
export function changeUserStatus(userId, version, status) {
  const data = {
    userId,
    version,
    status
  }
  return request({
    url: '/system/user/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return new Promise((resolve, reject) => {
    genKeyPair((uuid,public_key)=>{
      request({
        url: '/system/user/profile/updatePwd',
        method: 'put',
        headers: {
          isEncrypt: true,
          [encryptHeader]: uuid,
          publicKey: public_key,
        },
        data: data
      }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: data
  })
}

// 查询授权角色
export function getAuthRole(userId) {
  return request({
    url: '/system/user/authRole/' + userId,
    method: 'get'
  })
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: '/system/user/authRole',
    method: 'put',
    params: data
  })
}

// 查询部门下拉树结构
export function deptTreeSelect() {
  return request({
    url: '/system/user/deptTree',
    method: 'get'
  })
}
